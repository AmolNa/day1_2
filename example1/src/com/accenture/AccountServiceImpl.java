package com.accenture;


public class AccountServiceImpl implements AccountService{
	
	Account accounts[] = new Account[3];
	static int counter = 0;
	
	@Override
	public void addAccount(Account account) {
		accounts[counter] = account;
		counter ++; 

	}

	@Override
	public void displayAccounts() {
		for(Account acc: accounts) {
			System.out.println(acc);
		}
	}
}
