package com.accenture;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Date;

public class Demo {
	public static void main(String[] args) {
		Account sa = new SavingAccount("SA101", "dm101", 6000, 6.5f);
		Account la = new LoanAccount("HL501", "dm501", 1000000, 5, 8.9f);
		Account cc = new CreditCard("cc701", "dm701", 240000D, LocalDate.of(2022, 12, 10), LocalDate.of(2022, 12, 26) , 15.6f);
		System.out.println(sa);
		System.out.println(la);
		System.out.println(cc);
		
		System.out.println(sa.getInterest());
		System.out.println(la.getInterest());
		System.out.println(cc.getInterest());
		
		
//		AccountService as = new AccountServiceImpl();
//		as.addAccount(sa);
//		as.addAccount(la);
//		as.addAccount(cc);
//		as.displayAccounts();

	}
}
