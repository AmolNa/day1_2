package com.accenture;

public abstract class Account {
	String accNo;
	String name;
	public Account() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Account(String accNo, String name) {
		super();
		this.accNo = accNo;
		this.name = name;
	}
	public String getAccNo() {
		return accNo;
	}
	public void setAccNo(String accNo) {
		this.accNo = accNo;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "Account [accNo=" + accNo + ", name=" + name + "]";
	}
	
	abstract public float getInterest();
	
		
}
