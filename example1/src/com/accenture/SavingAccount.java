package com.accenture;

public class SavingAccount extends Account{
	private double balance;
	private float roi;
	public SavingAccount() {
		super();
	}
	public SavingAccount(String accNo, String name) {
		super(accNo, name);
	}
	public SavingAccount(String accNo, String name, double balance, float roi) {
		super(accNo, name);
		this.balance = balance;
		this.roi = roi;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	public float getRoi() {
		return roi;
	}
	public void setRoi(float roi) {
		this.roi = roi;
	}
	@Override
	public String toString() {
		return "SavingAccount [balance=" + balance + ", roi=" + roi + "]";
	}
	@Override
	public float getInterest() {
		return (float) ((balance*roi)/100);
	}
		
}
