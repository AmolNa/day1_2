package com.accenture;

public class LoanAccount extends Account{
	private double loanAmount;
	private int duration;
	private float roi;
	public LoanAccount() {
		super();
		// TODO Auto-generated constructor stub
	}
	public LoanAccount(String accNo, String name) {
		super(accNo, name);
		// TODO Auto-generated constructor stub
	}
	public LoanAccount(String accNo, String name, double loanAmount, int duration, float roi) {
		super(accNo, name);
		this.loanAmount = loanAmount;
		this.duration = duration;
		this.roi = roi;
	}
	public double getLoanAmount() {
		return loanAmount;
	}
	public void setLoanAmount(double loanAmount) {
		this.loanAmount = loanAmount;
	}
	public int getDuration() {
		return duration;
	}
	public void setDuration(int duration) {
		this.duration = duration;
	}
	public float getRoi() {
		return roi;
	}
	public void setRoi(float roi) {
		this.roi = roi;
	}
	@Override
	public String toString() {
		return "LoanAccount [loanAmount=" + loanAmount + ", duration=" + duration + ", roi=" + roi + "]";
	}
	@Override
	public float getInterest() {
		return (float) ((loanAmount*roi)/100);
	}
}
