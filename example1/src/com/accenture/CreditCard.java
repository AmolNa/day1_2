package com.accenture;

import java.time.LocalDate;
 
public class CreditCard extends Account{
	private double creditLimit;
	private LocalDate statementDate;
	private LocalDate dueDate;
	private float roi;
	public CreditCard() {
		super();
		// TODO Auto-generated constructor stub
	}
	public CreditCard(String accNo, String name) {
		super(accNo, name);
		// TODO Auto-generated constructor stub
	}
	
	


	
	public CreditCard(String accNo, String name, double creditLimit, LocalDate statementDate, LocalDate dueDate,
			float roi) {
		super(accNo, name);
		this.creditLimit = creditLimit;
		this.statementDate = statementDate;
		this.dueDate = dueDate;
		this.roi = roi;
	}
	@Override
	public String toString() {
		return "CreditCard [creditLimit=" + creditLimit + ", statementDate=" + statementDate + ", dueDate=" + dueDate
				+ ", roi=" + roi + "]";
	}
	public double getCreditLimit() {
		return creditLimit;
	}
	public void setCreditLimit(double creditLimit) {
		this.creditLimit = creditLimit;
	}
	public LocalDate getStatementDate() {
		return statementDate;
	}
	public void setStatementDate(LocalDate statementDate) {
		this.statementDate = statementDate;
	}
	public LocalDate getDueDate() {
		return dueDate;
	}
	public void setDueDate(LocalDate dueDate) {
		this.dueDate = dueDate;
	}
	public float getRoi() {
		return roi;
	}
	public void setRoi(float roi) {
		this.roi = roi;
	}
	@Override
	public float getInterest() {
		return 0.0f;
	}

	
	
	
}
