package com.hdfc;

import java.time.LocalDate;

public class Demo {
	public static void main(String[] args) {
		System.out.println("welcome to internet banking");

		Account acc = null; 
		
		acc = new SavingAccount("SA-201", "dm201", 5.6f, 12000, "supersavingaccount");
		System.out.println(acc);
		acc.getInterest();
		
		System.out.println("-----------------------------------------");
		acc = new CreditCard("CC-301", "dm301", 24.5f, "4319 2342", 
					LocalDate.of(2022,  12, 10), LocalDate.of(2022, 12, 01));
		System.out.println(acc);
		acc.getInterest();
		
		acc = new Loan("HL-401", "dm401", 8.9f, "542342", 3000000, 10);
		System.out.println(acc);
		acc.getInterest();

	}
}
