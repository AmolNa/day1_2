package com.hdfc;

import java.util.Date;

public  abstract  class Account {
	//property
	//instance variables
	private String accNo;
	private String name;
	private float roi;
	
	//constructors
	public Account() {
		System.out.println("account created at : " + new Date());
	}

	public Account(String accNo, String name, float roi) {
		super();
		this.accNo = accNo;
		this.name = name;
		this.roi = roi;
	}

	public String getAccNo() {
		return accNo;
	}

	//not relevant 
	public void setAccNo(String accNo) {
		this.accNo = accNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getRoi() {
		return roi;
	}

	public void setRoi(float roi) {
		this.roi = roi;
	}

	@Override
	public String toString() {
		return "Account [accNo=" + accNo + ", name=" + name + ", roi=" + roi + "]";
	}

	public abstract void getInterest();	
		//i will not implement but all child classes should implement this method 
	
}
