package com.hdfc;

public class Loan extends Account {
	private String loanNumber;
	private double loanAmount;
	private int loanDuration;

	public Loan() {
		super();
	}

	public Loan(String accNo, String name, float roi) {
		super(accNo, name, roi);
	}

	public Loan(String accNo, String name, float roi, String loanNumber, double loanAmount, int loanDuration) {
		super(accNo, name, roi);
		this.loanNumber = loanNumber;
		this.loanAmount = loanAmount;
		this.loanDuration = loanDuration;
	}

	public String getLoanNumber() {
		return loanNumber;
	}

	public void setLoanNumber(String loanNumber) {
		this.loanNumber = loanNumber;
	}

	public double getLoanAmount() {
		return loanAmount;
	}

	public void setLoanAmount(double loanAmount) {
		this.loanAmount = loanAmount;
	}

	public int getLoanDuration() {
		return loanDuration;
	}

	public void setLoanDuration(int loanDuration) {
		this.loanDuration = loanDuration;
	}

	@Override
	public String toString() {
		return "Loan [loanNumber=" + loanNumber + ", loanAmount=" + loanAmount + ", loanDuration=" + loanDuration
				+ ", getAccNo()=" + getAccNo() + ", getName()=" + getName() + ", getRoi()=" + getRoi() + "]";
	}
	@Override
	public void getInterest() {
		double interest = (loanAmount*loanDuration)/100;
		System.out.println("interest on home loan is : "+ interest);
	}

}
