package com.hdfc;

public interface AccountService {
	//declaration, no definition, essential details
	public void addAccount(Account acc );
	public void getAllAccount();
	public void getAccountByAccNo(String accNo);
}
