package com.hdfc;

import java.time.LocalDate;

public class CreditCard extends Account{
	private String ccNumber;
	private LocalDate dueDate;
	private LocalDate statementDate;
	public CreditCard() {
		super();
		// TODO Auto-generated constructor stub
	}
	public CreditCard(String accNo, String name, float roi) {
		super(accNo, name, roi);
		// TODO Auto-generated constructor stub
	}
	public CreditCard(String accNo, String name, float roi, String ccNumber, LocalDate dueDate,
			LocalDate statementDate) {
		super(accNo, name, roi);
		this.ccNumber = ccNumber;
		this.dueDate = dueDate;
		this.statementDate = statementDate;
	}
	public String getCcNumber() {
		return ccNumber;
	}
	public void setCcNumber(String ccNumber) {
		this.ccNumber = ccNumber;
	}
	public LocalDate getDueDate() {
		return dueDate;
	}
	public void setDueDate(LocalDate dueDate) {
		this.dueDate = dueDate;
	}
	public LocalDate getStatementDate() {
		return statementDate;
	}
	public void setStatementDate(LocalDate statementDate) {
		this.statementDate = statementDate;
	}
	@Override
	public String toString() {
		return "CreditCard [ccNumber=" + ccNumber + ", dueDate=" + dueDate + ", statementDate=" + statementDate
				+ ", getAccNo()=" + getAccNo() + ", getName()=" + getName() + ", getRoi()=" + getRoi() + "]";
	}
	
	@Override
	public void getInterest() {
		System.out.println("there is no interest in cc");	
	}
}
