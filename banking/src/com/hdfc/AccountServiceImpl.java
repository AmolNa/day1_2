package com.hdfc;

public class AccountServiceImpl implements AccountService {

	Account listOfAccount[] = new Account[5];
	int accountIndex = 0;

	@Override
	public void addAccount(Account acc) {
		listOfAccount[accountIndex] = acc;
		accountIndex++;
	}

	@Override
	public void getAllAccount() {
//		for(Account a : listOfAccount) {
//			System.out.println(a);
//		}
		for (int ai = 0; ai < accountIndex; ai++) {
			System.out.println(listOfAccount[ai]);
		}
	}

	@Override
	public void getAccountByAccNo(String accNo) {
		boolean  found = false;
		for (int ai = 0; ai < accountIndex; ai++) {
			if (listOfAccount[ai].getAccNo().equalsIgnoreCase(accNo)) {
				found = true;				//happy path
				System.out.println(listOfAccount[ai]);
				break;				
			}
		}
		if(found == false) {			//sad path
			System.out.println("no record found !!!");
		}
	}

}
