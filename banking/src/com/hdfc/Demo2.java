package com.hdfc;

import java.time.LocalDate;

public class Demo2 {
	public static void main(String[] args) {
		System.out.println("welcome to internet banking");

		//AccountService accService = new AccountServiceImpl();
		AccountService accService = new AccountServiceAdvImpl();
		Account acc = null; 
		
		acc = new SavingAccount("SA-201", "dm201", 5.6f, 12000, "supersavingaccount");
		accService.addAccount(acc);
		acc = new CreditCard("CC-301", "dm301", 24.5f, "4319 2342", 
					LocalDate.of(2022,  12, 10), LocalDate.of(2022, 12, 01));
		accService.addAccount(acc);
		acc = new Loan("HL-401", "dm401", 8.9f, "542342", 3000000, 10);
		accService.addAccount(acc);
		
		acc = new Loan("HL-402", "dm402", 8.5f, "56756756", 4000000, 5);
		accService.addAccount(acc);
		
		//print all account
		accService.getAllAccount();
		System.out.println("----------------");
		accService.getAccountByAccNo("SA-201231");
	}
}
