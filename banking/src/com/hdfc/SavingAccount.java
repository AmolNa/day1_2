package com.hdfc;

public class SavingAccount extends Account {
	// DRY ==> don't repeat yourself
	private double accountBalance;
	private String typeOfSavingAccout;

	public SavingAccount() {
		super();
	}

	public SavingAccount(String accNo, String name, float roi, double accountBalance, String typeOfSavingAccout) {
		super(accNo, name, roi);
		this.accountBalance = accountBalance;
		this.typeOfSavingAccout = typeOfSavingAccout;
	}

	public double getAccountBalance() {
		return accountBalance;
	}

	public void setAccountBalance(double accountBalance) {
		this.accountBalance = accountBalance;
	}

	public String getTypeOfSavingAccout() {
		return typeOfSavingAccout;
	}

	public void setTypeOfSavingAccout(String typeOfSavingAccout) {
		this.typeOfSavingAccout = typeOfSavingAccout;
	}

	@Override
	public String toString() {
		return "SavingAccount [accountBalance=" + accountBalance + ", typeOfSavingAccout=" + typeOfSavingAccout
				+ ", Account Number=" + getAccNo() + ", Name = " + getName() + ", ROI =" + getRoi() + "]";
	}

	@Override
	public void getInterest() {
		//write logic for calculating interest
		double interest = (accountBalance*getRoi())/100;
		System.out.println("interest is : "+ interest);
}
}
