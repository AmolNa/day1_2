package com.hdfc.composition;

public class Address {
	private String houseNo;
	private String buildingName;
	private String city;
	private String pin;
	public Address() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Address(String houseNo, String buildingName, String city, String pin) {
		super();
		this.houseNo = houseNo;
		this.buildingName = buildingName;
		this.city = city;
		this.pin = pin;
	}
	public String getHouseNo() {
		return houseNo;
	}
	public void setHouseNo(String houseNo) {
		this.houseNo = houseNo;
	}
	public String getBuildingName() {
		return buildingName;
	}
	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getPin() {
		return pin;
	}
	public void setPin(String pin) {
		this.pin = pin;
	}
	@Override
	public String toString() {
		return "Address [houseNo=" + houseNo + ", buildingName=" + buildingName + ", city=" + city + ", pin=" + pin
				+ "]";
	}
	
	
}
