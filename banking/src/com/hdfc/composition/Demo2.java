package com.hdfc.composition;

public class Demo2 {
	public static void main(String[] args) {
		//variable ==> value is subject to change
		//constant ==> value will remain same, we cannot update value
		int a = 10;		//non-final
		a++;
		final int b = 20 ;	//final / constant / fix value
		//b++;			//not possible
		
		
	}
}
