package com.hdfc.composition;

import java.util.Scanner;

public class Demo {
	public static void main(String[] args) {
		//create employee record and print it
		Address add =  new Address("A1-502", "Lakelife", "pune", "411046");
		Employee emp = new Employee(101, "dm101", add);
		System.out.println(emp);
		
		//accept input from user
		Scanner scan = new Scanner(System.in);
		int empId = scan.nextInt();
		String name = scan.next();			//if input does not contain space
		String houseNo = scan.next();
		String buildName = scan.next();
		String city = scan.next();
		String pin = scan.next();
		
		Address add1 = new Address(houseNo, buildName, city,  pin);
		Employee emp1 = new Employee(empId, name, add1);
		System.out.println(emp1);
	}
}
